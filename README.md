### Aalok Sathe's CV

Compiled binary can be accessed at [this link.](https://gitlab.com/aalok-sathe/cv/-/jobs/artifacts/master/raw/cvAalokSathe.pdf?job=compile_pdf)


### Repository structure
```bash
.
├─X cvAalokSathe.pdf
├── cvAalokSathe.tex
├── Makefile
├── modules
│   ├── abstracts.tex
│   ├── activities.tex
│   ├── awards.tex
│   ├── coursework.tex
│   ├── education.tex
│   ├── experience.tex
│   ├── grants.tex
│   ├── interests.tex
│   ├── publications.tex
│   ├── referees.tex
│   ├── skills.tex
│   ├── software.tex
│   └── talks.tex
├── readme
│   ├── LICENSE.txt
│   └── README.md
└── README.md
```